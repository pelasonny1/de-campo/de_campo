<?php

use PHPUnit\Framework\TestCase;

require __DIR__ . '/../internal/profuctFormatter.php';

class ProductFormatterTest extends TestCase
{
    public function testFormatPrecio()
    {
        $formatter = new ProductFormatter(20);

        $product = [
            'name' => 'Producto 1',
            'price' => 100
        ];

        $expectedOutput = [
            'name' => 'Producto 1',
            'price' => 100,
            'precioUSD' => '5.00'
        ];

        $this->assertEquals($expectedOutput, $formatter->formatProducto($product));
    }
}
