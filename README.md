
# De Campo a Campo

Este proyecto es una aplicación web construida con el framework Slim para PHP. Permite la gestión de productos, permitiendo operaciones CRUD (Crear, Leer, Actualizar, Eliminar) sobre los mismos.

## Requisitos Previos

Antes de comenzar, asegúrate de tener instalado lo siguiente en tu sistema:
- PHP 7.4 o superior
- MySQL o MariaDB
- Composer

## Configuración Inicial

Sigue estos pasos para configurar el entorno del proyecto:

### 1. Clonar el Repositorio

Clona este repositorio en tu máquina local usando:

```bash
git clone https://gitlab.com/pelasonny1/de-campo/de_campo
cd DeCampoACampo
```

### 2. Instalar Dependencias

Desde el directorio raíz del proyecto, ejecuta el siguiente comando para instalar todas las dependencias necesarias definidas en el archivo `composer.json`:

```bash
composer install
```

### 3. Configuración de la Base de Datos

Crea una base de datos en tu sistema de gestión de bases de datos MySQL/MariaDB y ejecuta el script ubicado en `sql/schema.sql` para crear las tablas necesarias. Puedes hacer esto desde la línea de comandos o usando una herramienta como phpMyAdmin.

```bash
mysql -u username -p database_name < sql/schema.sql
```

Asegúrate de actualizar las credenciales de conexión a la base de datos en `cfg/globals.php` con tus propios valores y, además, el precio del USD:

```php
const DB_HOST = 'tu_host';
const DB_NAME = 'nombre_de_tu_base_de_datos';
const DB_USER = 'tu_usuario';
const DB_PASS = 'tu_contraseña';
const USD_PRICE = "precio_usd"
```

### 4. Iniciar el Servidor Local

Para iniciar el servidor integrado de PHP y acceder a la aplicación, navega al directorio `public` y ejecuta:

```bash
cd public
php -S localhost:8090
```

Esto iniciará el servidor en `http://localhost:8090`, donde podrás acceder a la aplicación desde tu navegador.

## API Documentation

Esta API proporciona los siguientes puntos finales para administrar productos:

### GET /productos
- **Description**: Lista todos los productos.
- **Response**:
    - **Content-Type**: `application/json`
    - **Body**:
      ```json
      [
        {
          "id": int,
          "name": "string",
          "price": float,
          "price_usd": float
        }
      ]
      ```

### POST /addProductos
- **Description**: Agrega un producto.
- **Request**:
    - **Content-Type**: `application/json`
    - **Body**:
      ```json
      {
        "nombre": "string",
        "precio": float
      }
      ```
- **Response**:
    - **Content-Type**: `application/json`
    - **Body**:
      ```json
      {
        "id": int
      }
      ```

### PUT /productos/{id}
- **Description**: Actualiza un producto existente.
- **Request**:
    - **Content-Type**: `application/json`
    - **Body**:
      ```json
      {
        "nombre": "string",
        "precio": float
      }
      ```
- **Path Parameters**:
    - **id**: `int` (ID Producto a actualizar)
- **Response**:
    - **Content-Type**: `application/json`
    - **Body**:
      ```json
      {
        "success": "Producto actualizado con éxito"
      }
      ```

### DELETE /productos/{id}
- **Description**: Elimina un producto existente.
- **Path Parameters**:
    - **id**: `int` (ID Producto a eliminar)
- **Response**:
    - **Content-Type**: `application/json`
    - **Body**:
      ```json
      {
        "success": "Producto eliminado con éxito"
      }
      ```

## Uso de la Aplicación

Una vez que el servidor esté funcionando, podrás realizar operaciones CRUD a través de la interfaz de usuario proporcionada o mediante llamadas API directas a los endpoints definidos en `api.php`.
