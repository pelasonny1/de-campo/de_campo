<?php

use Controllers\ProductsController;
use Slim\App;

require __DIR__ . '/../internal/profuctFormatter.php';
require __DIR__ . '/../cfg/globals.php';
require __DIR__ . '/../internal/db.php';
require __DIR__ . '/../internal/ProductsController.php';

function addRoutes(App $app): void
{
    $container = $app->getContainer();
    $db = Database::getInstance()->getPdo();
    $productController = new ProductsController($db);

    $app->get('/productos', [$productController, 'getProducts']);

    $app->post('/addProductos', [$productController, 'addProduct']);

    $app->put('/productos/{id}', [$productController, 'updateProduct']);

    $app->delete('/productos/{id}', [$productController, 'deleteProduct']);

}

