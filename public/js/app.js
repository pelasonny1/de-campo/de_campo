function showMessage(message) {
    const messageContainer = document.getElementById('message-container');
    messageContainer.textContent = message;
    messageContainer.style.display = 'block';
    setTimeout(() => {
        messageContainer.style.display = 'none';
    }, 3000);
}

document.getElementById('productoForm').addEventListener('submit', function(e) {
    e.preventDefault();
    const productoId = document.getElementById('productoId').value;
    const nombre = document.getElementById('nombre').value.trim();
    const precio = document.getElementById('precio').value.trim();

    if (!nombre) {
        showMessage('Por favor, ingresa un nombre para el producto.');
        return;
    }

    if (!precio || isNaN(precio) || parseFloat(precio) <= 0) {
        showMessage('Por favor, ingresa un precio válido. Debe ser un número mayor que cero.');
        return;
    }

    const productoData = { nombre, precio };

    const method = productoId ? 'PUT' : 'POST';
    const url = productoId ? `/productos/${productoId}` : '/addProductos';

    fetch(url, {
        method: method,
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(productoData)
    })
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok.');
            }
            return response.json();
        })
        .then(data => {
            console.log('Success:', data);
            cargarProductos();
            document.getElementById('productoForm').reset();
            showMessage('¡Producto agregado con éxito!');
        })
        .catch(error => {
            console.error('Error:', error);
        });
});


function cargarProductos() {
    fetch('/productos')
        .then(response => {
            if (!response.ok) {
                throw new Error('Respuesta de red no fue ok');
            }
            return response.json();
        })
        .then(productos => {
            const container = document.getElementById('productosContainer');
            container.innerHTML = '';
            productos.forEach(producto => {
                container.innerHTML += `
                <div class="producto">
                    <input type="text" value="${producto.name}" class="editable" data-id="${producto.id}" data-field="nombre">
                    <input type="number" value="${producto.price}" class="editable" data-id="${producto.id}" data-field="precio">
                    <button onclick="guardarCambios(${producto.id})">Guardar</button>
                </div>`;
            });
        })
        .catch(error => {
            console.error('Error al cargar productos:', error);
        });
}


function eliminarProducto(id) {
    fetch(`/productos/${id}`, {
        method: 'DELETE'
    })
        .then(() => {
            console.log('Producto eliminado');
            cargarProductos();
            showMessage('Producto eliminado correctamente.');
        })
        .catch(error => {
            console.error('Error al eliminar producto:', error);
            showMessage('Error al eliminar producto. Consulta la consola para más detalles.');
        });
}
document.addEventListener('DOMContentLoaded', function() {
    cargarProductos(); // Carga inicial de productos
});

function cargarProductos() {
    fetch('/productos')
        .then(response => response.json())
        .then(productos => {
            const container = document.getElementById('productosContainer');
            container.innerHTML = '';
            productos.forEach(producto => {
                container.innerHTML += `
            <div class="producto">
                <input type="text" value="${producto.name}" class="editable" data-id="${producto.id}" data-field="nombre">
                <input type="number" value="${producto.price}" class="editable" data-id="${producto.id}" data-field="precio">
                <span> USD $${producto.precioUSD}</span>
                <button onclick="guardarCambios(${producto.id})">Guardar</button>
                <button onclick="eliminarProducto(${producto.id})">Eliminar</button>
            </div>`;
            });
        })
        .catch(error => {
            console.error('Error al cargar productos:', error);
        });
}

function guardarCambios(idProducto) {
    const nombre = document.querySelector(`input[data-id='${idProducto}'][data-field='nombre']`).value;
    const precio = document.querySelector(`input[data-id='${idProducto}'][data-field='precio']`).value;

    const productoData = { nombre, precio };

    fetch(`/productos/${idProducto}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(productoData)
    })
        .then(response => {
            if (!response.ok) {
                return response.json().then(data => { throw new Error(data.error || 'Error desconocido'); });
            }
            return response.json();
        })
        .then(data => {
            console.log('Producto actualizado:', data);
            cargarProductos();
            showMessage('Producto actualizado correctamente.');
        })
        .catch(error => {
            console.error('Error al actualizar el producto:', error);
            showMessage('Error al actualizar el producto. Consulta la consola para más detalles.');
        });
}

