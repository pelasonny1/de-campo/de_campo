<?php
class ProductFormatter {
    private $usdPrice;

    public function __construct($usdPrice) {
        $this->usdPrice = $usdPrice;
    }

    public function formatProducto($producto) {
        $producto['precioUSD'] = number_format($producto['price'] / $this->usdPrice, 2, '.', '');
        return $producto;
    }
}

