<?php

namespace Controllers;
use Exception;
use PDO;
use PDOException;
use ProductFormatter;

class ProductsController
{
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function getProducts($request, $response)
    {
        $formatter = new ProductFormatter(USD_PRICE);
        $stmt = $this->db->query('SELECT id, name, price FROM products');
        $products = $stmt->fetchAll();

        foreach ($products as $key => $product) {
            $products[$key] = $formatter->formatProducto($product);
        }

        $response->getBody()->write(json_encode($products));
        return $response->withHeader('Content-Type', 'application/json');
    }

    public function addProduct($request, $response)
    {
        $data = $request->getParsedBody();
        $nombre = $data['nombre'];
        $precio = $data['precio'];

        try {
            $stmt = $this->db->prepare('INSERT INTO products (name, price) VALUES (:nombre, :precio)');
            $stmt->bindValue(':nombre', $nombre);
            $stmt->bindValue(':precio', $precio);
            $stmt->execute();

            $id = $this->db->lastInsertId();
            $response->getBody()->write(json_encode(['id' => $id]));
            return $response->withHeader('Content-Type', 'application/json');
        } catch (PDOException $e) {
            $response->getBody()->write(json_encode(['error' => 'Error al agregar el producto']));
            return $response->withHeader('Content-Type', 'application/json')->withStatus(500);
        }
    }


    public function updateProduct($request, $response, $args)
    {
        $id = $args['id'];
        $data = $request->getParsedBody();
        $nombre = $data['nombre'];
        $precio = $data['precio'];

        try {
            $productoActualizado = $this->actualizarProducto($this->db, $id, $nombre, $precio);

            if ($productoActualizado) {
                $responseData = json_encode(['success' => 'Producto actualizado con éxito']);
                $response->getBody()->write($responseData);
                return $response->withHeader('Content-Type', 'application/json');
            } else {
                $responseData = json_encode(['error' => 'No se pudo actualizar el producto']);
                $response->getBody()->write($responseData);
                return $response->withHeader('Content-Type', 'application/json')->withStatus(500);
            }
        } catch (Exception $e) {
            $responseData = json_encode(['error' => 'Error del servidor: ' . $e->getMessage()]);
            $response->getBody()->write($responseData);
            return $response->withHeader('Content-Type', 'application/json')->withStatus(500);
        }
    }


    public function deleteProduct($request, $response, $args)
    {
        $id = $args['id'];
        $stmt = $this->db->prepare('DELETE FROM products WHERE id = :id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $response->getBody()->write(json_encode(['success' => 'Producto eliminado con éxito']));
        } else {
            $response->getBody()->write(json_encode(['error' => 'No se pudo eliminar el producto']));
            $response = $response->withStatus(500);
        }

        return $response->withHeader('Content-Type', 'application/json');
    }

    function actualizarProducto($pdo, $id, $nombre, $precio) {
        try {
            $sql = "UPDATE products SET name = :nombre, price = :precio WHERE id = :id";
            $stmt = $pdo->prepare($sql);
            $stmt->bindValue(':nombre', $nombre);
            $stmt->bindValue(':precio', $precio);
            $stmt->bindValue(':id', $id, PDO::PARAM_INT);

            return $stmt->execute();
        } catch (PDOException $e) {
            // Manejo de errores
            error_log("Error al actualizar producto: " . $e->getMessage());
            return false;
        }
    }
}

?>
